# About Me
Promonent languages: \
C, Python, JavaScript <!-- TODO: Replace language names with their icons -->

Some of my best (public) projects, in my opinion:
- [Passman](https://gitlab.com/mastertac/passman) - A simple password manager.
- [odin-website](https://gitlab.com/mastertac/odin-website) - A web page clone, a project in The Odin Project.
- [kingcobra.dev](https://gitlab.com/mastertac/kingcobra.dev) - A website for a tech business.
- [CodeSocial](https://gitlab.com/mastertac/CodeSocial) - A social platform for coders and programmers.
